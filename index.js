const express = require('express');
// Mongoose is a package that allows the creation of Schemas to model our data Structures
// Also has access to a number of methods for manipulating our database.
const mongoose = require('mongoose');
const app = express();
const port = 3001;

// MongoDB Atlas Connection
// When we want to use local mongoDB/robo3t
// mongoose.connect ('mongodb:localhost:27017/databaseName')
mongoose.connect('mongodb+srv://joshuagayanelo:12101991@cluster0.f5xck.mongodb.net/batch164_to-do?retryWrites=true&w=majority', 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
)
//Due to updates in mongodb drivers that allow connection to it, the default connection string is being flagged as an error
//By default a warning will be displayed in the terminal when the app is running, but this will not prevent mongoose from being used in the app.
//to prevent/avoid any current and future errors while connecting to Mongo DB, add the userNewUrlParser and useUnifiedTopology. -> see code above.


//Set notifications for connection success or failure
let db = mongoose.connection;
// connection error message
db.on('error', console.error.bind(console, "connection error"));
// Connection successful message
db.once('open', () => console.log(`We're connected to the cloud database.`))

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.listen(port, () => console.log(`Server running at port ${port}`));


// Mongoose Schemas
// Schemas determine the structure of the documents to be writtern in the database
// Act as blueprint to our data
// User Schema() constructor of the Mongoose module to create a new Schema Object

const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
		// Default values are the predefined values for a field if we don't put any value.
		default: 'pending'
	}

})

const Task = mongoose.model("Task", taskSchema)

// Routes/endpoints

// Creating a new task
// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return error.
	- If the task doesn't exists in the database, we add it in the database.
		1. The task data will be coming from the request's body.
		2. Create a new Task object with field/property.
		3. Save the new object to our database. 
*/

app.post('/tasks',(req, res) => {
	Task.findOne({ name: req.body.name }, (err, result) => {
		
		// Check if there are duplicate tasks
		// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
		// findOne() returns the first document that matches the search criteria
		// If there are no matches, the value of result is null
		// "err" is a shorthand naming convention for errors

		// If a document was found  and the document's name matches the information sent via the client:
		if(result != null && result.name == req.body.name) {
			return res.send("Duplicate task found.")
		} else {
			// if no document was found
			// create a new task and save it to the database.
			let newTask = new task ({
				name: req.body.name
			});

			// The "save" method will store the information to the database
			// Since the "newTask" was created/instantiated from the Mongoose Schema it will gain access to this method to save to the database
			// The "save" method will accept a callback function which stores any errors found in the first parameter
			// The second parameter of the callback function will store the newly saved document
			// Call back functions in mongoose methods are programmed this way to store any errors in the first parameter and the returned results in the second parameter

			newTask.save((saveErr, savedTask) => {
				// If there are erros in saving
				if(saveErr) {

					// Will print any errors found in the console
					// saveErr is an error object that will contain details about the error
					// Errors normally come as an object data type
					return console.error(saveErr)
				} else {
					// Return a status code of 201 for created
					// Sends a message "New task created" on successful creation
					return res.status(201).send("New task created.")
				}
			})
		}
	})
})

// Get all task
// Business Logic
/*
1. Find/retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send success back to the client and return an array of documents. 
*/

app.get('/tasks', (req, res) => {
	Task.find({}, (err, result) => {
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				dataFromMDB: result
			})
		}
	})
})



// USER MODEL
const userSchema = new mongoose.Schema({

	userName: String,
	password: String

})

const userSignup = mongoose.model("user", userSchema)

// SIGN UP
app.post('/signup',(req, res) => {
	
	userSignup.findOne({ userName: req.body.userName }, (err, result) => {

		if(result != null && result.userName == req.body.userName) {
			return res.send(`User ${req.body.userName} is already here. `)
		} else {
			
			let newUser = new userSignup({
				userName: req.body.userName,
				password: req.body.password
			})

			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr)
				} else {
					return res.status(201).send(`User ${req.body.userName} has been successfully created.`)
				}
			})

		}
	})
})

// GET USERS
app.get('/users',(req, res) => {

	userSignup.find({},(err, result) => {
		if(err) {
			return console.log(err)
		} else {
			return res.status(200).json({
				allUsers: result
			})
		}
	})

})


// GET SINGLE USER
app.get('/search-user', (req, res) => {

	userSignup.findOne({ userName: req.body.userName }, (err, result) => {

		if(result != null && result.userName == req.body.userName) {
			
			// let a = res.send(`Yes, user ${req.body.userName} is here.`)

			return res.status(200).json({
				user: result
			})

		} else {
			res.send(`Awww. \n Please try again brosis. \n Sheesh.`)
		}
	})
})

// GET BY USER ID
app.get('/get-by-id', (req, res) => {

	userSignup.findOne({ _id: req.body._id }, (err, result) => {

		if(result != null && result._id == req.body._id) {
			
			// let a = res.send(`Yes, user ${req.body.userName} is here.`)

			return res.status(200).json({
				user: result
			})

		} else {
			res.send(`Sorry paps. No hard feelings. \n Walang personalan. Trabaho lang.`)
		}
	})
})
